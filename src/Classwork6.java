import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Classwork6 {
    private static HashMap<String,Integer> map = new HashMap<>();
    Date date;

    public static void classworkkk(){
        map.put ("Первый", 1);
        map.put ("Второй", 2);
        map.put ("Третий", 3);
        map.put ("Четвертый", 4);
        map.put ("Пятый", 5);

        for (Map.Entry<String, Integer> pair : map.entrySet()) {
            String key = pair.getKey();
            int value = pair.getValue();
            System.out.println(key + " " + value);
        }
    }

    public void day1 () {
        String oldstring = "2018-10-26  15:00:12";
        SimpleDateFormat dataFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        try {
            date = dataFormat.parse(oldstring);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(day(date));
    }

    public long day (Date date) {
        Date today = new Date();
        return ((today.getTime() - date.getTime())/(1000*60*60*24));
    }
}
